Spree::Product.class_eval do
  scope :product_taxons, ->(product) { joins(:taxons).where(spree_taxons: { id: product.taxons.ids }) }
  scope :related_products, ->(product) { product_taxons(product).where.not(id: product.id).distinct }
  scope :order_by_available_on_desc, -> { order(available_on: :desc) }

  # 関連商品を返す
  def related_products(limit: nil)
    related_products = Spree::Product.includes(master: [:default_price, :images]).related_products(self)

    limit ? related_products.sample(limit) : related_products
  end

  # 新着商品のコレクションを返す
  def self.new_available_products(limit: nil)
    order_by_available_on_desc = includes(master: [:default_price, :images]).order_by_available_on_desc

    limit ? order_by_available_on_desc.first(limit) : order_by_available_on_desc
  end
end
