Spree::Taxon.class_eval do
  # Taxonに関連付けされている商品の総数を返す
  def products_count
    # 末端のTaxonの場合は商品数を返す
    return products.count if leaf?

    # 末端でない場合は紐付いた商品数を集計して返す
    leaves.includes(:products).map { |leaf| leaf.products.count }.sum
  end
end
