class Potepan::ProductsController < ApplicationController
  # 新着商品の表示数を設定
  RELATED_PRODUCTS_DISPLAY_LIMIT = 4

  def show
    # 商品データを取得
    @product = Spree::Product.find(params[:id])

    # 関連商品データを取得件数を指定して取得
    @related_products = @product.related_products(limit: RELATED_PRODUCTS_DISPLAY_LIMIT)
  end
end
