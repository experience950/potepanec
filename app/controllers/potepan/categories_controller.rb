class Potepan::CategoriesController < ApplicationController
  def show
    # Taxonomyを設定
    @taxonomies = Spree::Taxonomy.all.includes(:taxons)

    # Taxonを設定
    @taxon = Spree::Taxon.includes(:products).find_by_id(params[:id])

    # Taxonに属する商品をロード
    @products = load_products(@taxon)
  end

  private

  # Taxonに関連する商品を返す
  def load_products(taxon)
    taxon_ids = taxon.leaf? ? taxon.id : taxon.leaves.ids
    Spree::Product.joins(:taxons).where(spree_products_taxons: { taxon_id: taxon_ids })
  end
end
