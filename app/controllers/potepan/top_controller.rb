class Potepan::TopController < ApplicationController
  # 新着商品の表示数を設定
  NUMBER_DISPLAY_NEW_AVALABLE_PRODUCT = 10

  def index
    # 新着商品を取得
    @new_available_products = Spree::Product.new_available_products(limit: NUMBER_DISPLAY_NEW_AVALABLE_PRODUCT)
  end
end
