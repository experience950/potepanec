require 'rails_helper'

RSpec.feature "NewAvailableProducts", type: :feature do
  let!(:create_products) do
    create_list(:product, 4)
  end

  scenario "新着商品が正しく出力されること" do
    visit potepan_root_url
    within 'div.featuredProductsSlider' do
      expect(page).to have_content create_products[0].name
      expect(page).to have_content create_products[1].name
      expect(page).to have_content create_products[2].name
      expect(page).to have_content create_products[3].name
    end
  end
end
