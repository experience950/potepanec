require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  # 関連商品を取得
  describe "関連商品を取得したとき" do
    # 最大表示数を設定
    # ProductController::RELATED_PRODUCTS_DISPLAY_LIMITが元になっています。
    # 同定数の値に変更をくわえた場合、このDISPLAY_LIMIT定数も同じ値に変更してください。
    DISPLAY_LIMIT = 4

    # テストの主体となる商品を作成する
    let(:product) do
      create(:product) do |product|
        product.taxons << taxon_rails
        product.taxons << taxon_bag
      end
    end

    # テストで使用するTaxon群を作成
    let(:taxon_bag) do
      create(:taxon, name: "Bag")
    end

    let(:taxon_mug) do
      create(:taxon, name: "Mug")
    end

    let(:taxon_apache) do
      create(:taxon, name: "Apache")
    end

    let(:taxon_rails) do
      create(:taxon, name: "Rails")
    end

    let(:taxon_ruby) do
      create(:taxon, name: "Ruby")
    end

    # 関連商品を上限なしで取得
    let(:related_products) do
      product.related_products
    end

    # 関連商品を上限ありで取得
    let(:related_products_with_limit) do
      product.related_products(limit: DISPLAY_LIMIT)
    end

    context "関連商品が存在しないケース" do
      context "最大表示数を指定せず実行" do
        it "0個の商品が取得できること" do
          expect(related_products.count).to eq 0
        end
      end
    end

    context "関連商品が2個登録されているケース" do
      # 関連商品（カテゴリー）を2個作成する
      let!(:apache_bag_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_bag
        end
      end

      # 関連のない商品を2個作成する
      let!(:apache_mug_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_mug
        end
      end

      context "最大表示数を指定せず実行" do
        it "2個の商品が取得できること" do
          expect(related_products.count).to eq 2
        end

        it "正しい商品が取得できること" do
          expect(related_products.to_a.sort).to be == apache_bag_products.sort
          expect(related_products.to_a.sort).to_not be == apache_mug_products.sort
        end
      end

      context "最大表示数を指定して実行" do
        it "取得した商品数が最大表示数以下であること" do
          expect(related_products_with_limit.count).to be <= DISPLAY_LIMIT
        end
      end
    end

    context "関連商品が4個登録されているケース" do
      # 関連商品（カテゴリー）を2個作成する
      let!(:apache_bag_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_bag
        end
      end

      # 関連のない商品を2個作成する
      let!(:apache_mug_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_mug
        end
      end

      # 関連商品（ブランド）を2個作成する
      let!(:rails_mug_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_rails
          product.taxons << taxon_mug
        end
      end

      context "最大表示数を指定せず実行" do
        it "4個の商品が取得できること" do
          expect(related_products.count).to eq 4
        end

        it "正しい商品が取得できること" do
          expect(related_products.to_a.sort).to be == (apache_bag_products + rails_mug_products).sort
          expect(related_products.to_a.sort).to_not be == apache_mug_products.sort
        end
      end

      context "最大表示数を指定して実行" do
        it "取得した商品数が最大表示数以下であること" do
          expect(related_products_with_limit.count).to be <= DISPLAY_LIMIT
        end
      end
    end

    context "関連商品が6個登録されているケース" do
      # 関連商品（カテゴリー）を2個作成する
      let!(:apache_bag_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_bag
        end
      end

      # 関連のない商品を2個作成する
      let!(:apache_mug_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_apache
          product.taxons << taxon_mug
        end
      end

      # 関連商品（ブランド）を2個作成する
      let!(:rails_mug_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_rails
          product.taxons << taxon_mug
        end
      end

      # 関連商品かつ、ブランドが2つ紐付いている商品を2個作成する
      let!(:multiple_brand_products) do
        create_list(:product, 2) do |product|
          product.taxons << taxon_bag
          product.taxons << taxon_rails
          product.taxons << taxon_ruby
        end
      end

      context "最大表示数を指定せず実行" do
        it "6個の商品が取得できること" do
          expect(related_products.count).to eq 6
        end

        it "正しい商品が取得できること" do
          expect(related_products.to_a.sort).to be == (apache_bag_products + rails_mug_products + multiple_brand_products).sort
          expect(related_products.to_a.sort).to_not be == apache_mug_products.sort
        end
      end

      context "最大表示数を指定して実行" do
        it "取得した商品数が最大表示数以下であること" do
          expect(related_products_with_limit.count).to be <= DISPLAY_LIMIT
        end
      end
    end
  end

  describe "新着商品を取得したとき" do
    let!(:create_products) do
      create_list(:product, 2, available_on: date_near) |
      create_list(:product, 2, available_on: date_last_month) |
      create_list(:product, 2, available_on: date_last_year)
    end

    let(:date_near) do
      Time.zone.local(2018, 4, 1)
    end

    let(:date_last_month) do
      Time.zone.local(2018, 3, 31)
    end

    let(:date_last_year) do
      Time.zone.local(2017, 4, 2)
    end

    context "表示数を指定しない場合" do
      it "正しい件数が取得できること" do
        expect(Spree::Product.new_available_products.count).to eq 6
      end

      it "正しい順番で取得できること" do
        new_available_products = Spree::Product.new_available_products
        expect(new_available_products[0].available_on).to eq date_near
        expect(new_available_products[1].available_on).to eq date_near
        expect(new_available_products[2].available_on).to eq date_last_month
        expect(new_available_products[3].available_on).to eq date_last_month
        expect(new_available_products[4].available_on).to eq date_last_year
        expect(new_available_products[5].available_on).to eq date_last_year
      end
    end

    context "表示数を指定した場合" do
      it "正しい件数が取得できること" do
        display_num = 4
        expect(Spree::Product.new_available_products(limit: display_num).count).to eq 4
      end

      it "正しい順番で取得できること" do
        new_available_products = Spree::Product.new_available_products
        expect(new_available_products[0].available_on).to eq date_near
        expect(new_available_products[1].available_on).to eq date_near
        expect(new_available_products[2].available_on).to eq date_last_month
        expect(new_available_products[3].available_on).to eq date_last_month
      end
    end
  end
end
